package com.health.insurence.quote;

public class HealthInsurenceQuoteService {
	public String getQuote(String namePerson, String personGender, String personAge, String personHypertension,
			String personBloodPressure, String personBloodSugar, String personOverweight, String personDailyExercise,
			String personSmoking, String personAlcohol, String personDrugs) {
		int age = 0;
		double premium = 0;
		try {
			String gender = null, hypertension = null, bloodPressure = null, bloodSugar = null, overweight = null,
					dailyExercise = null;
			String smoking = null, alcohol = null, drugs = null, name = null;
			name = namePerson;
			gender = personGender;
			if (personAge != null) {
				age = Integer.parseInt(personAge);
			}
			hypertension = personHypertension;
			bloodPressure = personBloodPressure;
			bloodSugar = personBloodSugar;
			overweight = personOverweight;
			dailyExercise = personDailyExercise;
			smoking = personSmoking;
			alcohol = personAlcohol;
			drugs = personDrugs;

			System.out.println("name,gender,age" + name + "" + gender + "" + age);
			if (age >= 1)
				premium = 5000;
			if (age >= 2)
				premium = premium + (premium * 0.1);
			if (age >= 3)
				premium = premium + (premium * 0.1);
			if (age >= 4)
				premium = premium + (premium * 0.1);
			if (age >= 5)
				premium = premium + (premium * 0.1);
			if (age >= 6)
				premium = premium + (premium * 0.2);

			if ("M".equalsIgnoreCase(gender)) {
				premium = premium + (premium * 0.02);
			}
			if ("Y".equalsIgnoreCase(hypertension)) {
				premium = premium + (premium * 0.01);
			}
			if ("Y".equalsIgnoreCase(bloodPressure)) {
				premium = premium + (premium * 0.01);
			}
			if ("Y".equalsIgnoreCase(bloodSugar)) {
				premium = premium + (premium * 0.01);
			}
			if ("Y".equalsIgnoreCase(overweight)) {
				premium = premium + (premium * 0.01);
			}
			if ("Y".equalsIgnoreCase(dailyExercise)) {
				premium = premium - (premium * 0.03);
			}
			if ("Y".equalsIgnoreCase(smoking)) {
				premium = premium + (premium * 0.03);
			}
			if ("Y".equalsIgnoreCase(alcohol)) {
				premium = premium + (premium * 0.03);
			}
			if ("Y".equalsIgnoreCase(drugs)) {
				premium = premium + (premium * 0.03);
			}

			System.out.println("Health Insurance Premium for Mr." + name + ":" + premium);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return String.valueOf(premium);

	}

}
