package com.health.insurence.quote;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/health")
public class HealthInsurenceQuote {

	@POST
	@Path("/{param}")
	public Response getMsg(@FormParam("name") String name, @FormParam("gender") String gender,
			@FormParam("personAge") String personAge, @FormParam("hypertension") String hypertension,
			@FormParam("bloodpressure") String bloodpressure, @FormParam("bloodsugar") String bloodsugar,
			@FormParam("overweight") String overweight, @FormParam("smoking") String smoking,
			@FormParam("alcohol") String alcohol, @FormParam("dailyexercise") String dailyexercise,
			@FormParam("drugs") String drugs) {
		System.out.println(name);
		HealthInsurenceQuoteService quote = new HealthInsurenceQuoteService();
		String output = "The Health insurence quote for : " + quote.getQuote(name, gender, personAge, hypertension,
				bloodpressure, bloodsugar, overweight, smoking, alcohol, dailyexercise, drugs);
		return Response.status(200).entity(output).build();
	}
}