<%-- <%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> --%>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Health Insurance</title>
</head>
<body>
	<div>
		<h1>Health Insurance Premium Generator</h1>
		<form action="rest/health/quote" method="post">
			<table align="center">
				<tr>
					<td><h3>Please enter name</h3> <input type="text" name="name"
						value="" maxlength="30" required /></td>
				</tr>
				<tr>
					<td><h3>Please select gender</h3>&nbsp;&nbsp;
					<select
						id="gender" name="gender" required>
							<option value="M">Male</option>
							<option value="F">Female</option>
							<option value="O">Other</option>
					</select></td>
				</tr>
				<tr>
					<td><h3>Please select the age</h3>&nbsp;&nbsp;
					<select
						id="personAge" name="personAge" required>
							<option value="1">less than 18</option>
							<option value="2">18-25</option>
							<option value="3">25-30</option>
							<option value="4">30-35</option>
							<option value="5">35-40</option>
							<option value="6">greater than 40</option>
					</select></td>
				</tr>
				<div id="cssmenu">
					<tr>
						<td><h3>Current health condition</h3>
							<ul>
								<li>Hypertension&nbsp;&nbsp;
								<select id="hypertension"
									name="hypertension" required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Blood pressure&nbsp;&nbsp;
								<select id="bloodpressure"
									name="bloodpressure" required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Blood sugar&nbsp;&nbsp;
								<select id="bloodsugar"
									name="bloodsugar" required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Overweight&nbsp;&nbsp;
								<select id="overweight"
									name="overweight" required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
							</ul></td>
					</tr>
				</div>
				<div id="cssmenu">
					<tr>
						<td><h3>Habits</h3>
							<ul>
								<li>Smoking&nbsp;&nbsp;
								<select id="smoking" name="smoking"
									required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Alcohol&nbsp;&nbsp;
								<select id="alcohol" name="alcohol"
									required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Daily exercise&nbsp;&nbsp;
								<select id="dailyexercise"
									name="dailyexercise" required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
								<li>Drugs&nbsp;&nbsp;
								<select id="drugs" name="drugs"
									required>
										<option value="Y">Yes</option>
										<option value="N">No</option>
								</select></li>
								<br />
							</ul></td>
					</tr>
					<div>
						<tr>
							<td><button name="submit">submit</button></td>
						</tr>
			</table>
		</form>
	</div>
</body>
</html>
